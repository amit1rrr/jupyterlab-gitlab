# Changelog

## Version 0.1.1 (2018-12-18)

- Fix browsing of projects with name that differs from path

## Version 0.1.0 (2018-12-12)

- Initial release
